//
//  ArticleDetailViewController.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit
import Photos

final class ArticleDetailViewController: UIViewController {
  
  // MARK: - Private properties
  @IBOutlet private weak var imageView: UIImageView!
  private let viewModel: ArticleDetailViewModel = ArticleDetailViewModel()
  
  
  // MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigationItems()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    imageView.loadImageUsingCacheWithURLString(viewModel.link)
  }
  
  // MARK: - Private functions
  private func setupNavigationItems() {
    let rightBarButtonItem = UIBarButtonItem(title: viewModel.rightButtonItemName, style: .plain, target: self, action: #selector(actionSave))
    navigationItem.rightBarButtonItem = rightBarButtonItem
  }
  
  // MARK: - Public functions
  func setup(_ link: String) {
    viewModel.link = link
  }
  
  // MARK: - Actions
  @IBAction func actionSave() {
    GalleryManager.requestStatus { [weak self] in
      guard let welf = self else { return }
      if let image = welf.imageView.image {
        UIImageWriteToSavedPhotosAlbum(image, welf, #selector(welf.image(_:didFinishSavingWithError:contextInfo:)), nil)
      }
    }
  }
  
  @objc private func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    if let error = error {
      AlertMessage.show(sender: self, messageText: error.localizedDescription)
    } else {
      AlertMessage.show(sender: self, messageText: Constants.Messages.imageSaved)
    }
  }
}
