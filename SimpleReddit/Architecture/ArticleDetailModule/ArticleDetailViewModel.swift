//
//  ArticleDetailViewModel.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

protocol ArticleDetailViewModelProtocol {
  var rightButtonItemName: String { get }
}

final class ArticleDetailViewModel: NSObject {
  
  // MARK: - Public properties
  var link: String = ""

}

// MARK: - ArticleDetailViewModelProtocol
extension ArticleDetailViewModel: ArticleDetailViewModelProtocol {
  var rightButtonItemName: String { return "Save" }
}
