//
//  ArticleCell.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit


protocol ArticleCellProtocol {
  func title(at index: Int) -> NSAttributedString?
  func dateTitle(at index: Int) -> NSAttributedString?
  func commentsText(at index: Int) -> NSAttributedString?
  func imageUrl(at index: Int) -> String?
}

final class ArticleCell: UICollectionViewCell {
  
  //MARK: - Private properties
  @IBOutlet private weak var containerWidthConstraint: NSLayoutConstraint!
  @IBOutlet private weak var previewImageView: UIImageView!
  @IBOutlet private weak var containerView: UIView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var dateLabel: UILabel!
  @IBOutlet private weak var commentLabel: UILabel!
  private var viewModel: ArticleCellProtocol?
  
  
  //MARK: - Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.contentView.translatesAutoresizingMaskIntoConstraints = false
    containerWidthConstraint.constant = UIScreen.main.bounds.width
    containerView.layer.borderWidth = 1
    containerView.layer.borderColor = UIColor.lightGray.cgColor
  }
  
  // MARK: - Public functions
  func setup(_ viewModel: ArticleCellProtocol, at index: Int) {
    self.viewModel = viewModel
    titleLabel.attributedText = viewModel.title(at: index)
    dateLabel.attributedText = viewModel.dateTitle(at: index)
    commentLabel.attributedText = viewModel.commentsText(at: index)
    
    if let imageURL = viewModel.imageUrl(at: index) {
      previewImageView.loadImageUsingCacheWithURLString(imageURL)
    }
  }
}
