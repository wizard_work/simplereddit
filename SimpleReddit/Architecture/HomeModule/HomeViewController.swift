//
//  HomeViewController.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit


final class HomeViewController: UIViewController {
  
  // MARK: - Private properties
  @IBOutlet private weak var collectionView: UICollectionView! //TODO: - IGListKit be better
  private let refresher: UIRefreshControl = UIRefreshControl()
  private let viewModel: HomeViewModel = HomeViewModel()
  
  
  // MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    viewModel.delegate = self
    setupUI()
    viewModel.loadArticles()
  }
  
  // MARK: - Private functions
  private func setupUI() {
    registerCells()
    setupRefreshControl()
    if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      let height: CGFloat = CGFloat(viewModel.estimatedCellHeight)
      flowLayout.estimatedItemSize = CGSize(width: view.bounds.size.width, height: height)
    }
  }
  private func setupRefreshControl() {
    refresher.tintColor = UIColor.red
    refresher.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)
    collectionView.addSubview(refresher)
    collectionView.alwaysBounceVertical = true
  }
  private func registerCells() {
    for item in viewModel.registerCells {
      collectionView.register(UINib(nibName: item.nibName, bundle: nil), forCellWithReuseIdentifier: item.identifier)
    }
  }
  
  // MARK: - Actions
  @objc func actionRefresh() {
    viewModel.loadArticles()
  }
}

// MARK: - UICollectionViewDataSource
extension HomeViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.numberItemsInSection(section)
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(ArticleCell.self, for: indexPath)
    cell.setup(viewModel, at: indexPath.row)
    return cell
  }
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    if indexPath.row >= viewModel.objectsToDisplay.count - 1 { 
      viewModel.loadArticles(reload: false)
    }
  }
}

// MARK: - UICollectionViewDelegate
extension HomeViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.actionDidSelect(at: indexPath)
  }
}

// MARK: - HomeViewModelDelegate
extension HomeViewController: HomeViewModelDelegate {
  func homeViewModelEndRefresh(_ viewModel: HomeViewModel) {
    refresher.endRefreshing()
  }
  func homeViewModelDidReloadData(_ viewModel: HomeViewModel) {
    collectionView.reloadData()
  }
  
  func homeViewModel(_ viewModel: HomeViewModel, didUpdateWith insertIndexPaths: [IndexPath], scrollOffsetY: Double) {
    viewModel.scrollOffsetY = Double(collectionView.contentOffset.y)
    collectionView.performBatchUpdates({
      collectionView.insertItems(at: insertIndexPaths)
    }, completion: { (finished) in
      DispatchQueue.main.async { [weak self] in
        guard let welf = self else { return }
        welf.collectionView.contentOffset = CGPoint(x: 0, y: viewModel.scrollOffsetY)
      }
    })
  }
  
  func homeViewModel(_ viewModel: HomeViewModel, didError message: String) {
    AlertMessage.show(sender: self, messageText: message)
  }
  
  func homeViewModel(_ viewModel: HomeViewModel, shouldOpenImageBy link: String) {
    let controller = ArticleDetailViewController.controllerFromStoryboard(.main)
    controller.setup(link)
    show(controller, sender: self)
  }
}

