//
//  HomeViewModel.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

protocol HomeViewModelDelegate: class {
  func homeViewModel(_ viewModel: HomeViewModel, didError message: String)
  func homeViewModel(_ viewModel: HomeViewModel, shouldOpenImageBy link: String)
  func homeViewModel(_ viewModel: HomeViewModel, didUpdateWith insertIndexPaths: [IndexPath], scrollOffsetY: Double)
  func homeViewModelDidReloadData(_ viewModel: HomeViewModel)
  func homeViewModelEndRefresh(_ viewModel: HomeViewModel)
}

protocol HomeViewModelProtocol {
  var registerCells: [(nibName: String, identifier: String)] { get }
  func numberItemsInSection(_ section: Int) -> Int
}


final class HomeViewModel {
  
  // MARK: - Private properties
  private var nextLink: String?
  private var isLoading: Bool = false
  private var startIndex: Int = 0
  
  // MARK: - Public properties
  let estimatedCellHeight: Double = 300.0 /// photo height, labels height
  var scrollOffsetY: Double = 0
  var objectsToDisplay: [Article] = []
  weak var delegate: HomeViewModelDelegate?
  
  
  // MARK: - Private functions
  private func resetToDefault() {
    isLoading = false
    nextLink = nil
    startIndex = 0
    scrollOffsetY = 0.0
    objectsToDisplay.removeAll()
  }
  
  private func update(with reload: Bool, insertIndexPaths: [IndexPath] ) {
    delegate?.homeViewModelEndRefresh(self)
    
    if reload {
      delegate?.homeViewModelDidReloadData(self)
    } else {
      self.delegate?.homeViewModel(self, didUpdateWith: insertIndexPaths, scrollOffsetY: self.scrollOffsetY)
    }
    
    isLoading = false
  }
  
  private func updateData(_ response: ArticlesResponse, reload: Bool) {
    if reload {
      resetToDefault()
    }
    
    let array = response.items.sorted(by: { (s1, s2) -> Bool in return s1.createdData > s2.createdData })
    objectsToDisplay.append(contentsOf: array)
    nextLink = response.nextLink
    
    let lastIndex = objectsToDisplay.count - 1
    let insertIndexPaths = Array(startIndex...lastIndex).map { IndexPath(item: $0, section: 0) }
    
    DispatchQueue.main.async { [weak self] in
      self?.update(with: reload, insertIndexPaths: insertIndexPaths)
    }
    
    startIndex = lastIndex + 1
  }
  
  // MARK: - Public functions
  func loadArticles(reload: Bool = true) {
    guard isLoading == false else { return }
    isLoading = true
    
    if reload {
      nextLink = nil
    }
    
    NetworkManager.shared.getTopArticles(with: self, completionHandler: { [weak self] (result) in
      guard let welf = self else { return }
      
      do {
        let response = try result()
        welf.updateData(response, reload: reload)
      } catch {
        welf.delegate?.homeViewModel(welf, didError: (error as! NetworkError).errorMessage())
      }
    })
  }
  
  // MARK: - Actions
  func actionDidSelect(at indexPath: IndexPath) {
    let article = objectsToDisplay[indexPath.row]
    delegate?.homeViewModel(self, shouldOpenImageBy: article.thumbnailURL)
  }
}

// MARK: - ArticleCellProtocol
extension HomeViewModel: ArticleCellProtocol {
  func imageUrl(at index: Int) -> String? {
    let article = objectsToDisplay[index]
    return article.thumbnailURL
  }
  func title(at index: Int) -> NSAttributedString? {
    let article = objectsToDisplay[index]
    return article.title.apply(font: .systemFont(ofSize: 18), color: .black)
  }
  func dateTitle(at index: Int) -> NSAttributedString? {
    let article = objectsToDisplay[index]
    return "\(article.authorFullname): \(article.createdData.timeAgoSinceDate())".apply(font: .boldSystemFont(ofSize: 14), color: .darkGray)
  }
  func commentsText(at index: Int) -> NSAttributedString? {
    let article = objectsToDisplay[index]
    return "Comments: \(article.commentCount)".apply(font: .systemFont(ofSize: 12), color: .darkGray)
  }
}

// MARK: - HomeViewModelProtocol
extension HomeViewModel: HomeViewModelProtocol {
  var registerCells: [(nibName: String, identifier: String)] { return [("ArticleCell", "ArticleCell")] }
  
  func numberItemsInSection(_ section: Int) -> Int {
    return objectsToDisplay.count
  }
}

// MARK: - NetworkManagerArticlesProtocol
extension HomeViewModel: NetworkManagerArticlesProtocol {
  var link: String? { return nextLink }
}
