//
//  Article.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

struct Article: Decodable {
  private var created: Double = 0.0
  var thumbnailURL: String = ""
  var title: String = ""
  var authorFullname: String = ""
  var commentCount: Int = 0
  var createdData: Date { return Date(timeIntervalSince1970: created) }
  
  // MARK: - JSON
  enum DataCodingKeys: String, CodingKey {
    case data
  }
  
  enum CodingKeys: String, CodingKey {
    case title
    case thumbnail
    case author
    case commentCount = "num_comments"
    case created
  }
  
  // MARK: - Lifecycle
  init(from decoder: Decoder) throws {
    let dataContainer = try decoder.container(keyedBy: DataCodingKeys.self)
    let container = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
    title = try container.decode(String.self, forKey: .title)
    thumbnailURL = try container.decode(String.self, forKey: .thumbnail)
    authorFullname = try container.decode(String.self, forKey: .author)
    commentCount = try container.decode(Int.self, forKey: .commentCount)
    created = try container.decode(Double.self, forKey: .created)
  }
}
