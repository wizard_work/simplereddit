//
//  ArticlesResult.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 05.06.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

struct ArticlesResponse: Decodable {
  let items: [Article]
  var nextLink: String?
  
  // MARK: - JSON
  enum CodingKeys: String, CodingKey {
    case data = "data"
  }
  enum ChildrenCodingKeys: String, CodingKey {
    case items = "children"
    case nextLink = "after"
  }
  
  // MARK: - Lifecycle
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let childrenContainer = try container.nestedContainer(keyedBy: ChildrenCodingKeys.self, forKey: .data)
    self.items = try childrenContainer.decode([Article].self, forKey: .items)
    self.nextLink = try childrenContainer.decode(String.self, forKey: .nextLink)
  }
}
