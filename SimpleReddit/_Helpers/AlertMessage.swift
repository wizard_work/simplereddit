//
//  AlertMessage.swift
//
//  Created by Yurii Goroshenko on 4/7/17.

import UIKit

//Example:
/*
 1)
 AlertMessage.show(sender: self, messageText: "<text>")
 
 2)
 let actionCancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
 let actionSettings = UIAlertAction(title: "settings", style: .default, handler: { _ in
 UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
 })
 AlertMessage.show(sender: self, messageText: "please_allow_camera_permission", actions: [actionCancel, actionSettings])
 */

struct AlertTextField {
  var cancelTitle: String?
  var sendTitle: String?
  var title: String?
  var message: String?
  var placeholder: String?
  var keyboardType: UIKeyboardType
}

final class AlertMessage {
  
  // MARK: - Private properties
  private static let actionOk = "OK"
  
  
  // MARK: - Private functions
  private static func presentDefaultAlert(_ alert: UIAlertController, sender: UIViewController) {
    let action = UIAlertAction(title: AlertMessage.actionOk, style: .default, handler: nil)
    alert.addAction(action)
    
    DispatchQueue.main.async {
      sender.present(alert, animated: true, completion:nil)
    }
  }
  
  // MARK: - Public functions
  static func show(sender: UIViewController?, title: String? = "", messageText: String?, actions: [UIAlertAction]? = nil, style: UIAlertController.Style = .alert) {
    guard let sender = sender else { return }
    
    let alert = UIAlertController(title: title, message: messageText,  preferredStyle: style)
    guard let actions = actions else {
      presentDefaultAlert(alert, sender: sender)
      return
    }
    
    for action in actions {
      alert.addAction(action)
    }
    
    DispatchQueue.main.async {
      sender.present(alert, animated: true, completion:nil)
    }
  }
  
  static func showWithTextField(sender: UIViewController?, property: AlertTextField, completion: @escaping ((String?) -> Void)) {
    guard let sender = sender else { return }
    
    let alert = UIAlertController(title: property.title, message: property.message,  preferredStyle: .alert)
    let actionCancel = UIAlertAction(title: property.cancelTitle, style: .cancel, handler: nil)
    let actionSend = UIAlertAction(title: property.sendTitle, style: .default, handler: { _ -> Void in
      let textField = alert.textFields![0] as UITextField
      completion(textField.text)
    })
    
    alert.addAction(actionCancel)
    alert.addAction(actionSend)
    alert.addTextField { (textField : UITextField!) -> Void in
      textField.placeholder = property.placeholder
      textField.keyboardType = property.keyboardType
    }
    
    DispatchQueue.main.async {
      sender.present(alert, animated: true, completion:nil)
    }
  }
}
