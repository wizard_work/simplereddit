//
//  Constants.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

typealias CompletionHandler = () -> Void

enum Storyboards: String {
  case main = "Main"
}

struct Constants {
  struct Messages {
    static let imageSaved: String = "Image has been saved to photos."
  }
  
  // MARK: - Errors
  struct Errors {
    static let server: String = "Server error: %d"
    static let failMIMEURL: String = "Data has fail MIME type"
    static let failURL: String = "Error: URL is not valid"
    static let failData: String = "Data error"
    static let failJSON: String = "JSON fail"
    static let failLoadImage: String = "Problem with load image"
  }
}
