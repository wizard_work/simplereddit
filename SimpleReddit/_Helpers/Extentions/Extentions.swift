//
//  Extentions.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

// MARK: - String
extension String {
  func apply(font: UIFont, color: UIColor) -> NSAttributedString {
    var attributes: [NSAttributedString.Key: Any] = Dictionary()
    attributes[NSAttributedString.Key.font] = font
    attributes[NSAttributedString.Key.foregroundColor] = color
    return NSAttributedString(string: self, attributes: attributes)
  }
}

// MARK: - NSObject
extension NSObject {
  class var nameOfClass: String {
    return NSStringFromClass(self).components(separatedBy: ".").last!
  }
}

// MARK: - Date
extension Date {
  private func convertInterval(_ interval: Int, key: String) -> String {
    return interval == 1 ? "\(interval) \(key) ago" : "\(interval) \(key)s ago"
  }
  
  func timeAgoSinceDate() -> String {
    
    let fromDate = self
    let toDate = Date()
    
    // Year
    if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
      return convertInterval(interval, key: "year")
    }
    
    // Month
    if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
      return convertInterval(interval, key: "month")
    }
    
    // Day
    if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
      return convertInterval(interval, key: "day")
    }
    
    // Hours
    if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
      return convertInterval(interval, key: "hour")
    }
    
    // Minute
    if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
      return convertInterval(interval, key: "minute")
    }
    
    return "a moment ago"
  }
}

// MARK: - UIView
extension UIView {
  class var nibName: String {
    return "\(self)".components(separatedBy: ".").first ?? ""
  }
}

// MARK: - UIViewController
extension UIViewController {
  private class func instantiateControllerInStoryboard<T: UIViewController>(_ storyboard: UIStoryboard, identifier: String) -> T {
    return storyboard.instantiateViewController(withIdentifier: identifier) as! T
  }
  class func controllerFromStoryboard(_ storyboard: Storyboards) -> Self {
    let namedStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
    return instantiateControllerInStoryboard(namedStoryboard, identifier: nameOfClass)
  }
}

// MARK: - UICollectionView
extension UICollectionView {
  func dequeueReusableCell<T: UICollectionViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
    return dequeueReusableCell(withReuseIdentifier: T.nibName, for: indexPath) as! T
  }
}
