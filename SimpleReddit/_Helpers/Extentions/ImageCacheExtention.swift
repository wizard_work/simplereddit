//
//  Constants.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

private let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
  func loadImageUsingCacheWithURLString(_ URLString: String) {
    if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
      DispatchQueue.main.async {
        self.image = cachedImage
      }
      return
    }
    
    guard let url = URL(string: URLString) else { return }
    
    URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
      guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else { return }
      guard let data = data, let downloadedImage = UIImage(data: data) else { return }
      
      imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
      DispatchQueue.main.async {
        self.image = downloadedImage
      }
    }).resume()
  }
}
