//
//  GalleryManager.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit
import Photos

final class GalleryManager {
  
  // MARK: - Private functions
  private static func openSettings() {
    if let url = URL(string: UIApplication.openSettingsURLString) {
      if UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
    }
  }
  
  // MARK: - Public functions
  static func requestStatus(completion: @escaping CompletionHandler) {
    PHPhotoLibrary.requestAuthorization { status in
      DispatchQueue.main.async {
        switch status {
        case .notDetermined:
          print("")
        case .restricted:
          openSettings()
        case .denied:
          openSettings()
        case .authorized:
          completion()
        @unknown default:
          print("")
        }
      }
    }
  }
}
