//
//  NetworkManager.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 01.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

enum NetworkError: Error {
  case error(code: Int)
  case failMIMEURL
  case failJSON
  case failData
  case unowned
  
  func errorMessage() -> String {
    switch self {
    case .failJSON: return Constants.Errors.failJSON
    case .failMIMEURL: return Constants.Errors.failMIMEURL
    case .failData: return Constants.Errors.failData
    case .error(let code): return String(format: Constants.Errors.server, code) 
    case .unowned: return ""
    }
  }
}

protocol NetworkManagerArticlesProtocol {
  var link: String? { get }
}

final class NetworkManager {
  static let shared = NetworkManager()
  
  //MARK: - Private properties
  private static let kRequestGetTopList: String = "https://www.reddit.com/top.json"
  private static let maxLimit: Int = 20
  private lazy var operationQueue: OperationQueue = OperationQueue()
  
  
  //MARK: - Private functions
  private func request(with urlString: String, completionHandler: @escaping OperationCompletionHandler<Data?>) {
    guard let url = URL(string: urlString) else { return }
    let operation = NetworkManagerOperation(url: url, completion: completionHandler)
    operationQueue.addOperation(operation)
  }
  
  //MARK: - Public functions
  func cancelOperation(by url: URL) {
    guard let operations = operationQueue.operations as? [NetworkManagerOperation] else { return }
    if let operation = operations.filter({ $0.url == url }).first {
      operation.cancel()
    }
  }
  
  func cancelAllOperations() {
    operationQueue.cancelAllOperations()
  }
  
  //MARK: - API`s
  func getTopArticles(with object: NetworkManagerArticlesProtocol, completionHandler: @escaping OperationCompletionHandler<ArticlesResponse>) {
    var link = NetworkManager.kRequestGetTopList + "?limit=\(NetworkManager.maxLimit)"
    if let nextLink = object.link {
      link += "&after=\(nextLink)"
    }
    
    request(with: link) { (result) in
      do {
        let value = try result()
        guard let json = value else { return completionHandler { throw NetworkError.failJSON } }
        let response = try! JSONDecoder().decode(ArticlesResponse.self, from: json)
        completionHandler { return response }
      } catch {
        completionHandler { throw error }
      }
    }
  }
}
