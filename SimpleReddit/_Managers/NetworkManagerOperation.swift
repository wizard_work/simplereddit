//
//  NetworkManagerOperation.swift
//  SimpleReddit
//
//  Created by Yurii Goroshenko on 06.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import Foundation

typealias OperationCompletionHandler<T> = (_ result: () throws -> T) -> Void

final class NetworkManagerOperation: Operation {

  //MARK: - Properties
  private static let mimeType: String = "application/json"
  var url: URL
  var completion: OperationCompletionHandler<Data?>
  
  // MARK: - Lifecycle
  init(url: URL, completion: @escaping OperationCompletionHandler<Data?>) {
    self.url = url
    self.completion = completion
  }
  
  override func main() {
    let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, taskError) in
      do {
        try NetworkManagerOperation.validationRequest(data, response: response)
        self.completion{ return data }
      } catch {
        self.completion{ throw error }
      }
    })
    
    task.resume()
  }
  
  //MARK: - Private functions
  private static func validationRequest(_ data: Data?, response: URLResponse?) throws {
    guard data != nil else { throw NetworkError.failData }
    
    guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
      let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
      throw NetworkError.error(code: statusCode)
    }
    
    guard let mime = httpResponse.mimeType, mime == NetworkManagerOperation.mimeType else {
      throw NetworkError.failMIMEURL
    }
  }
}
